import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;import javax.print.attribute.standard.DateTimeAtCompleted;

public class Genetic 
{
	static Random r = new Random();
	public static void main(String[] args)
	{
		for(int i = 0; i < 100; i++)
		{
			double d = r.nextDouble()*200-100;
			ins.add(d);
			outs.add(10d);
		}
		for(double d : ins)
			//outs.add(Math.sqrt(d)+d*2+1);
			
		defaultMap.put("X", 0d);
		
		new Genetic();
	}
	static List<Double> ins = new ArrayList<Double>();
	static List<Double> outs = new ArrayList<Double>();
	static HashMap<String, Double> defaultMap = new HashMap<String, Double>(); 
	public Node GetRandomNode(int maxDepth)
	{
		Random r = new Random();
		String[] ts = new String[]{
				Variable.class.getName(),
				Constant.class.getName(),
				Absolute.class.getName(),
				Sqrt.class.getName(),
				Squared.class.getName(),
				Add.class.getName(),
				Minus.class.getName(),
				Divide.class.getName(),
				Multiply.class.getName()
				};
		String t;
		if(maxDepth == 0)
			t = ts[r.nextInt(2)];
		else
			t = ts[r.nextInt(ts.length)];
		Node n;
		int childrenNeeded = 0;
		if(t.equals(Variable.class.getName()))
		{
			List<String> l = new ArrayList<String>(defaultMap.keySet());
			String name = l.get(new Random().nextInt(l.size()));
			n = new Variable(name, defaultMap);
		}
		else if(t.equals(Constant.class.getName()))
		{
			n = new Constant(Math.round((r.nextDouble()*20-10)*(r.nextDouble() < 0.5 ? -1 : 1)));
		}
		else if(t.equals(Absolute.class.getName()))
		{
			n = new Absolute();
			childrenNeeded = 1;
		}
		else if(t.equals(Sqrt.class.getName()))
		{
			n = new Sqrt();
			childrenNeeded = 1;
		}
		else if(t.equals(Squared.class.getName()))
		{
			n = new Squared();
			childrenNeeded = 1;
		}
		else if(t.equals(Add.class.getName()))
		{
			n = new Add();
			childrenNeeded = 2;
		}
		else if(t.equals(Minus.class.getName()))
		{
			n = new Minus();
			childrenNeeded = 2;
		}
		else if(t.equals(Divide.class.getName()))
		{
			n = new Divide();
			childrenNeeded = 2;
		}
		else if(t.equals(Multiply.class.getName()))
		{
			n = new Multiply();
			childrenNeeded = 2;
		}
		else
		{
			System.out.println("ERR");
			return null;
		}
		for(int j = 0; j < childrenNeeded; j++)
			n.ns.add(GetRandomNode(maxDepth - 1));
		return n;
	}
	public Genetic()
	{
		//HashMap<String, Double> map = new HashMap<String, Double>(defaultMap);
		List<Organism> os = new ArrayList<Organism>();
		for(int i = 0; i < 1000; i++)
		{
			Organism o = new Organism(map);
			os = NoDuplicates(os);
			os.add(o);
		}
		for(int generation = 0; generation < 1; generation++)
		{
			os = NoDuplicates(os);
			for(Organism o : os)
				o.calculate();
			os.sort(new OrganismComparator());
			int nWinners = 10;
			int nEntrants = 100;
			
			System.out.println("Generation "+generation);
			for(Organism w : os)
				System.out.println((String.format("%6s", Math.round(w.sumSquaredError)))+":\t"+w);
			System.out.println();
			
			List<Organism> winners = new ArrayList<Organism>();
			winners.addAll(Tournament(os, nWinners, nEntrants));
			
			nWinners = winners.size();
			List<Organism> crossOvers;
			Organism best = os.get(0);
			os.clear();
			
			/*
			System.out.println("Generation "+generation);
			for(Organism w : winners)
				System.out.println((String.format("%6s", Math.round(w.sumSquaredError)))+":\t"+w);
			System.out.println();	
			*/
			
			for(int i = 0; i < 100; i++)
			{
				crossOvers = CrossOver(winners.get(r.nextInt(nWinners)), winners.get(r.nextInt(nWinners)), 1000/nWinners);				
				os.addAll(crossOvers);
			}
			os = NoDuplicates(os);
			
			
			//Take the worst 10% and discard them.
			{
				os.sort(new OrganismComparator());
				Collections.reverse(os);
				for(int i = 0; i < os.size()/10; i++)
					os.remove(0);
				Collections.reverse(os);
			}
			while(os.size() < 1000)
			{
				os.add(new Organism(Mutate(os.get(r.nextInt((int)Math.sqrt(os.size()))).root)));
			}
			
			//ReAdd the best of last time
			os.remove(0);
			os.add(best);
		}
	}
	public List<Node> GetAllNodes(Node root)
	{
		Queue<Node> q = new LinkedList<Node>();
		q.add(root);
		List<Node> l = new ArrayList<Node>();
		while(!q.isEmpty())
		{
			Node n = q.poll();
			List<Node> el = n.ns;
			for(int i = 0; i < el.size(); i++)
			{
				if(q .contains(el.get(i))) q.add(el.get(i));
				if(el.contains(el.get(i))) l.add(el.get(i));
			}
		}
		return l;
	}
	public List<Organism> NoDuplicates(List<Organism> os_)
	{
		List<Organism> os = new ArrayList<Organism>();
		List<String> usedIds = new ArrayList<String>();
		for(Organism o : os_)
		{
			String id = o.toString();
			if(!usedIds.contains(id))
			{
				os.add(o);
				usedIds.add(id);
			}
		}
		return os;
	}
	public List<Organism> Tournament(List<Organism> os, int nWinners, int nEntrants)
	{
		List<Organism> winners = new ArrayList<Organism>();
		if(os.size() == 0) return winners;
		//for(int i = 0; i < nWinners; i++)
		while(winners.size() < nWinners)
		{
			List<Organism> entrants = new ArrayList<Organism>();
			for(int j = 0; j < nEntrants; j++)
				entrants.add(os.get(r.nextInt(os.size())));
			entrants.sort(new OrganismComparator());
			winners.add(entrants.get(0));
		}
		winners = NoDuplicates(winners);
		return winners;
	}
	public List<Organism> CrossOver(Organism o1, Organism o2, int nCrossOvers)
	{
		List<Organism> os = new ArrayList<Organism>();
		Random r = new Random();
		for(int i = 0; i < nCrossOvers; i++)
		{
			//todo: make this better
			int rand = r.nextInt(2);
			Organism ro1 = new Organism(rand == 0 ? o1.root.clone() : o2.root.clone());
			Organism ro2 = new Organism(rand == 0 ? o2.root.clone() : o1.root.clone());
			if(ro1.root.ns.size() == 0)
				os.add(r.nextFloat() < 0.5 ? ro1 : ro2);
			else
			{
				Node root = ro1.root;
				List<Node> allNodes1 = GetAllNodes(root);
				List<Node> allNodes2 = GetAllNodes(ro2.root);
				Collections.shuffle(allNodes1);
				Collections.shuffle(allNodes2);
				for(Node n : allNodes1)
				{
					if(n.ns.size() == 0 || allNodes2.size() == 0)
						continue;
					n.ns.set(n.ns.size()-1, allNodes2.get(0));
					os.add(new Organism(root));
					break;
				}
			}
		}
		os = NoDuplicates(os);
		return os;
	}
	public Node Mutate(Node n)
	{
		Node clone = n.clone();
		List<Node> allNodes = GetAllNodes(n.clone());
		Collections.shuffle(allNodes);
		for(Node e : allNodes)
		{
			if(e.ns.size() == 0)
				continue;
			e.ns.set(e.ns.size()-1, GetRandomNode(4));
			return clone;
		}
		return GetRandomNode(4);
	}
	public class Organism
	{
		Node root;
		HashMap<String, Double> map;
		public Organism(HashMap<String, Double> map_)
		{
			map = new HashMap<String, Double>(map_);
			Random r = new Random();
			root = GetRandomNode(2+r.nextInt(2));
		}
		public Organism(Node r)
		{
			map = new HashMap<String, Double>();
			root = r;
		}
		public String toString()
		{
			return root+"";
		}
		public Double sumSquaredError = 0d; 
		public double calculate()
		{
			sumSquaredError = 0d;
			List<Double> sses = new ArrayList<Double>();
			//System.out.println(":"+root);
			for(int i = 0; i < ins.size(); i++)
			{
				map.clear();
				map.put("X", ins.get(i));
				double rv = root.value();
				double dv = outs.get(i);
				double f = (rv-dv);
				f *= f;
				sses.add(f);
				//System.out.println(":"+f);
				sumSquaredError += f;
			}
			//System.out.println(":");
			return sumSquaredError = !sumSquaredError.isNaN() ? sumSquaredError : Double.MAX_VALUE;
		}
	}
	static class OrganismComparator implements Comparator<Organism>
	{
		@Override
		public int compare(Organism o1, Organism o2) {
			return o1.sumSquaredError.compareTo(o2.sumSquaredError);
		}
	}
	public abstract class Node
	{
		public List<Node> ns = new ArrayList<Node>();
		public abstract double value();
		public double get(int index)
		{
			if(ns.size() > index)
				return ns.get(index).value();
			else
				return 0;
		}
		public final Node clone()
		{
			Node c = clone_();
			List<Node> nsc = new ArrayList<Node>();
			for(Node n : ns)
			{
				Node nc = n.clone();
				nsc.add(nc);
			}
			c.ns = nsc;
			return c;
		}
		protected abstract Node clone_();
		public String toString()
		{
			String result = "";
			for(Node n : ns)
				result += " "+n.toString()+" ";
			return " ["+id()+" "+result+"] ";
		}
		public String id(){ return "?"; }
	}
	public class Variable extends Node
	{
		public Variable(String n, HashMap<String, Double> m){ name = n; map = m;}
		String name;
		protected HashMap<String, Double> map;
		public double value()
		{
			if(map.containsKey(name))
				return map.get(name);
			return 0;
		}
		public Node clone_()
		{
			Node n = new Variable(name, map); 
			return n;
		}
		public String toString()
		{
			return " ["+id()+" "+name+" ] ";
		}
		public String id(){ return "v"; }
	}
	public class Constant extends Node
	{
		public Constant(double v){ this.v = v; }
		double v;
		public double value()
		{
			return v;
		}
		public Node clone_()
		{
			Node n = new Constant(v); 
			return n;
		}
		public String toString()
		{
			return " ["+id()+" "+v+"] ";
		}
		public String id(){ return "c"; }
	}
	public abstract class Function extends Node
	{
		
	}
	public class Absolute extends Function
	{
		@Override
		public double value() {
			return Math.abs(get(0));
		}
		public Node clone_()
		{
			Node n = new Absolute();
			return n;
		}
		public String id(){ return "ABS"; }
	}
	public class Sqrt extends Function
	{
		@Override
		public double value() {
			return Math.sqrt(get(0));
		}
		public Node clone_()
		{
			Node n = new Sqrt();
			return n;
		}
		public String id(){ return "SQRT"; }
	}
	public class Squared extends Function
	{
		@Override
		public double value() {
			return Math.pow(get(0), 2);
		}
		public Node clone_()
		{
			Node n = new Squared();
			return n;
		}
		public String id(){ return "SQR"; }
	}
	public class Add extends Function
	{
		public double value()
		{
			return get(0)+get(1);
		}
		public Node clone_()
		{
			Node n = new Add();
			return n;
		}
		public String id(){ return "+"; }
	}
	public class Minus extends Function
	{
		public double value()
		{
			return get(0)-get(1);
		}
		public Node clone_()
		{
			Node n = new Minus();
			return n;
		}
		public String id(){ return "-"; }
	}
	public class Multiply extends Function
	{
		public double value()
		{
			return get(0)*get(1);
		}
		public Node clone_()
		{
			Node n = new Multiply();
			return n;
		}
		public String id(){ return "*"; }
	}
	public class Divide extends Function
	{
		public double value()
		{
			if(get(1) != 0)
				return get(0)/get(1);
			else
				return 0;
		}
		public Node clone_()
		{
			Node n = new Divide();
			return n;
		}
		public String id(){ return "/"; }
	}
}
